CREATE TABLE `USER` (                                     *
  `idUtente` INT NOT NULL AUTO_INCREMENT,                 *
  `email` VARCHAR(45) NOT NULL,
  `tipoUtente` VARCHAR(45) NOT NULL,
  `nome` VARCHAR(45) NULL,
  `cognome` VARCHAR(45) NULL,
  `hashedPassword` VARCHAR(90) NULL,
  `salt` VARCHAR(90) NULL,
  `dataPrimaRegistrazione` DATE NULL,
  `dataCompletamentoRegistrazione` DATE NULL,
  `chiaveRegistrazione` VARCHAR(90) NULL,
  `idReferral` INT NULL,
  PRIMARY KEY (`idUtente`)
);

CREATE TABLE `HISTORY_COUPON` (
  `idHistoryCoupon` INT NOT NULL AUTO_INCREMENT,
  `codiceCoupon` VARCHAR(45) NOT NULL,
  `puntiCoupon` INT NULL,
  `idUtente` INT NOT NULL,
  PRIMARY KEY (`idHistoryCoupon`),
  FOREIGN KEY (`idUtente`) REFERENCES USER(`idUtente`)
);