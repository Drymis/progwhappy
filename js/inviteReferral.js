var request;
var nInput;

//al caricamento della pagina, valorizza il testo contente i punti coupon
$( document ).ready(function() {
    addEmailInput();
    //alert($.cookie("uuid"));
    if(typeof $.cookie("uuid") === 'undefined'){
        window.location.href = '../pages/landing.html';
    }

    //$.removeCookie("puntiCoupon");
    //document.cookie = "puntiCoupon" + '=; expires=Thu, 01-Jan-1970 00:00:01 GMT;';
    //$.cookie('puntiCoupon',null, { path: '/' });

    request = $.ajax({
        url: "../php/puntiCouponHistory.php",
        type: "post"
    });

    // Success callback handler
    request.done(function (response, textStatus, jqXHR){
        $('#puntiCoupon').text($.cookie("puntiCoupon"));
    });

    // Error callback handler 
    request.fail(function (jqXHR, textStatus, errorThrown){
        afterDBAction(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });

});

//al submit del form contenente le email chiama inviteReferral.php
$("#mailInputForm").submit(function(event){

    // Prevent default posting of form - put here to work in case of errors
    event.preventDefault();

    // Abort any pending request
    if (request) {
        request.abort();
    }

    var $form = $(this);

    var $inputs = $form.find("input, select, button, textarea");

    // Serialize the data in the form
    var serializedData = $form.serialize();

    // Let's disable the inputs for the duration of the Ajax request.
    // Note: we disable elements AFTER the form data has been serialized.
    // Disabled form elements will not be serialized.
    $inputs.prop("disabled", true);

    request = $.ajax({
        url: "../php/inviteReferral.php",
        type: "post",
        data: serializedData
    });

    // Success callback handler
    request.done(function (response, textStatus, jqXHR){
        afterDBAction(response);
    });

    // Error callback handler 
    request.fail(function (jqXHR, textStatus, errorThrown){
        afterDBAction(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs
        $inputs.prop("disabled", false);
    });

});

//al click sui punti coupon, dopo aver chiamato puntiCouponHistory.php, apre una modale con la history dei punti coupon
$('#puntiCoupon').click(function(){

    request = $.ajax({
        url: "../php/puntiCouponHistory.php",
        type: "post"
    });

    // Success callback handler
    request.done(function (response, textStatus, jqXHR){
        $("#historyPuntiCouponContent").html(response);
        $("#historyPuntiCouponModal").modal("show");
    });

    // Error callback handler 
    request.fail(function (jqXHR, textStatus, errorThrown){
        afterDBAction(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });

});

//Aggiunge un input per le mail
function addEmailInput(){
    nInput=1;
    $(":input").each(function(i, val) {
        if (~$(this).attr('type').indexOf("text")) {
            nInput++;
          }
    });
    var htmlInputTemplate='<div class="form-group" id="mailInputForm'+nInput+'"><label class="control-label col-sm-2" for="mailInput'+nInput+'">Email amico n.'+nInput+' </label><div class="col-sm-10"><input type="text" class="form-control" id="mailInput'+nInput+'" name="mailInput'+nInput+'" value="" placeholder="Inserire email"/></div></div>';
    $('#mailInputs').append(htmlInputTemplate);
}

//Rimuove l'ultimo input per le mail
function removeEmailInput(){
    nInput=0;
    $(":input").each(function(i, val) {
        if (~$(this).attr('type').indexOf("text")) {
             nInput++;
        }
    });
    if(nInput>1){
        $( '#mailInputForm'+nInput ).remove();
    }
}

//in base alla response decide cosa mostrare nell'alert
//sostituisce un contenitore con il testo della response
function afterDBAction(response){

    if(~response.indexOf("Amici invitati, avvisali!")){
        $('.myAlert-bottom').removeClass('alert-danger');
        $('.myAlert-bottom').addClass('alert-success');
        $('#formContent').html('<label class="control-label col-sm-offset-3 col-sm-5">'+response+'</label>');
        response = 'Successo! Inviti inviati.';
    }else{
        $('.myAlert-bottom').removeClass('alert-success');
        $('.myAlert-bottom').addClass('alert-danger');
        response = 'Errore: '+response;
    }
    
    $("#alertpopup").html(response);
    $(".myAlert-bottom").show();
    setTimeout(function(){
        $(".myAlert-bottom").hide(); 
    }, 5000);

}