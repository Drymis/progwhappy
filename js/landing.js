  var request;

  //al submit del form contenente l'email chiama landing.php
  $("#mailInputForm").submit(function(event){

    // Prevent default posting of form - put here to work in case of errors
    event.preventDefault();

    // Abort any pending request
    if (request) {
        request.abort();
    }

    var $form = $(this);

    var $inputs = $form.find("input, select, button, textarea");

    // Serialize the data in the form
    var serializedData = $form.serialize();

    // Let's disable the inputs for the duration of the Ajax request.
    // Note: we disable elements AFTER the form data has been serialized.
    // Disabled form elements will not be serialized.
    $inputs.prop("disabled", true);

    request = $.ajax({
        url: "../php/landing.php",
        type: "post",
        data: serializedData
    });

    // Success callback handler
    request.done(function (response, textStatus, jqXHR){
        afterDBAction(response);
    });

    // Error callback handler 
    request.fail(function (jqXHR, textStatus, errorThrown){
        afterDBAction(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs
        $inputs.prop("disabled", false);
    });

});

//in base alla response decide cosa mostrare nell'alert
//sostituisce un contenitore con il testo della response
function afterDBAction(response){

    if(~response.indexOf("Controlla la tua e-mail per verificare la registrazione!")){
        $('.myAlert-bottom').removeClass('alert-danger');
        $('.myAlert-bottom').addClass('alert-success');
        $('#formContent').html('<label class="control-label col-sm-offset-3 col-sm-5">'+response+'</label>');
        response = 'Successo! Email inviata.';
    }else{
        $('.myAlert-bottom').removeClass('alert-success');
        $('.myAlert-bottom').addClass('alert-danger');
        response = 'Errore: '+response;
    }
    
    $("#alertpopup").text(response);
    $(".myAlert-bottom").show();
    setTimeout(function(){
        $(".myAlert-bottom").hide(); 
    }, 3500);

}