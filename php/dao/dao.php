<?php

    function connectToDB(){
        
		$servername = "localhost";
		$username = "id8566129_whappytesting";
		$password = "5YHufMsF7uax";
		$dbname = "id8566129_whappydbtesting";
		
		$connection = new mysqli($servername, $username, $password, $dbname);

		if ($connection->connect_error) {
			die("Connection failed: " . $connection->connect_error);
		} 
		
		return $connection;

    }
    
    function preventSQLInjection($conn, &...$fields){
        foreach ($fields as &$field) {
            $field = mysqli_real_escape_string($conn, $field);
        }
    }

    // verifica esistenza email di registrazione 
    // I: email
    // O: true/false
    function existsEmailRegistration($email){
        
        try{
            $conn = connectToDB();

            preventSQLInjection($conn, $email);
            
            $sql = "SELECT USER.email FROM USER WHERE USER.email = '".$email."'";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            $conn->close();
        }
    }

    // crea un'utenza di tipo prospect/prospect-referral
    // I: email, chiave registrazione, codice coupon, tipologia prospect, id referente
    // O: "Success" oppure Descrizione errore
    function saveUser($email, $chiaveRegistrazione, $codiceCoupon, $tipoUtente, $idReferente){

        try{
            $conn = connectToDB();

            preventSQLInjection($conn, $chiaveRegistrazione, $codiceCoupon, $tipoUtente, $idReferente);

            $sql = "INSERT INTO USER (idUtente, email, tipoUtente, nome, cognome, hashedPassword, salt, dataPrimaRegistrazione, dataCompletamentoRegistrazione, chiaveRegistrazione, idReferral)
                    VALUES (0, '".$email."', '".$tipoUtente."', NULL, NULL, NULL, NULL, NOW(), NULL, '".$chiaveRegistrazione."', ".$idReferente.")";

            if ($conn->query($sql) === TRUE) {
                $idUtente = $conn->insert_id;

                $sql1 = "INSERT INTO HISTORY_COUPON (idHistoryCoupon, codiceCoupon, puntiCoupon, idUtente)
                        VALUES (0, '".$codiceCoupon."' , NULL, ".$idUtente.")";

                if ($conn->query($sql1) === TRUE) {
                    return "Success";
                } else {
                    return "Error: " . $sql1 . "<br>" . $conn->error;
                }
            } else {
                return "Error: " . $sql . "<br>" . $conn->error;
            }
        } finally {
            $conn->close();
        }

    } 

    // verifica esistenza email di registrazione 
    // I: email
    // O: true/false
    function existsRegistrationKey($chiaveRegistrazione){

        try{
            $conn = connectToDB();

            preventSQLInjection($conn, $chiaveRegistrazione);

            $sql = "SELECT USER.email FROM USER WHERE USER.chiaveRegistrazione = '".$chiaveRegistrazione."'";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            $conn->close();
        }

    }

    // verifica se l'utente è registrato, se sì ritorna alcuni dati precedentemente valorizzati
    // I: chiave registrazione
    // O: false oppure un oggetto contenente i campi già valorizzati a DB
    function retrieveRegistrationInfo($chiaveRegistrazione){

        try{
            $conn = connectToDB();
            $queryResultFields = array();
            $idUtente=null;
            
            preventSQLInjection($conn, $chiaveRegistrazione);

            $sql = "SELECT USER.idUtente, USER.email, USER.dataCompletamentoRegistrazione FROM USER 
                    WHERE USER.chiaveRegistrazione = '".$chiaveRegistrazione."' AND USER.tipoUtente='UTENTE'";

            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                while($r = $result->fetch_assoc()) {
                    $idUtente = $r["idUtente"];
                    $queryResultFields = $r;
                }

                $sql1 = "SELECT HISTORY_COUPON.codiceCoupon, HISTORY_COUPON.puntiCoupon FROM HISTORY_COUPON
                        WHERE HISTORY_COUPON.idUtente=".$idUtente;

                $result1 = $conn->query($sql1);

                if ($result1->num_rows > 0) {
                    while($r1 = $result1->fetch_assoc()) {
                        $queryResultFields = array_merge($queryResultFields, $r1);
                    }
                }

                return $queryResultFields;

            } else{
                return false;
            }
        } finally {
            $conn->close();
        }

    }

    // completa la registrazione dell'utente, verificando che non sia già stata completata in precedenza
    // I: nome, cognome, chiave registrazione
    // O: true/false
    function saveCompletedRegistration($name, $surname, $hashedPassword, $salt, $chiaveRegistrazione){

        try{
            $conn = connectToDB();
            $idUtente = null;

            preventSQLInjection($conn, $name, $surname, $hashedPassword, $salt, $chiaveRegistrazione);

            $sql = "UPDATE USER SET USER.nome='".$name."', USER.cognome='".$surname."', USER.hashedPassword='".$hashedPassword."', USER.salt='".$salt."', USER.dataCompletamentoRegistrazione=NOW(), USER.tipoUtente='UTENTE' 
                    WHERE USER.chiaveRegistrazione='".$chiaveRegistrazione."' AND USER.tipoUtente<>'UTENTE'";
                    
            if ($conn->query($sql) === TRUE) {
                
                $sql1 = "SELECT USER.idUtente FROM USER WHERE USER.chiaveRegistrazione='".$chiaveRegistrazione."' AND USER.tipoUtente='UTENTE'";
                        
                $result1 = $conn->query($sql1);
        
                if ($result1->num_rows > 0) {
                    while($r1 = $result1->fetch_assoc()) {
                        $idUtente = $r1["idUtente"];
                    }
                }
        
                $sql2 = "UPDATE HISTORY_COUPON SET HISTORY_COUPON.puntiCoupon=100 WHERE HISTORY_COUPON.idUtente=".$idUtente;

                if ($conn->query($sql2) === TRUE) {
                    return true;
                } else{
                    return false;
                }
            } else {
                return false;
            }
        } finally {
            $conn->close();
        }

    }

    // aggiunge punti coupon al referente
    // I: id referente, punti coupon da aggiungere
    // O: "Success" oppure Descrizione errore
    function addPuntiCoupon($idReferente, $puntiCouponDaAggiungere){

        try{
            $conn = connectToDB();

            preventSQLInjection($conn, $idReferente, $puntiCouponDaAggiungere);

            $sql = "UPDATE HISTORY_COUPON SET HISTORY_COUPON.puntiCoupon=HISTORY_COUPON.puntiCoupon+".$puntiCouponDaAggiungere." 
                    WHERE HISTORY_COUPON.idUtente='".$idReferente."'";

            if ($conn->query($sql) === FALSE) {
                return "Error updating record: " . $conn->error;
            }else{
                return "Success";
            }
        } finally {
            $conn->close();
        }

    }

    // estrae le email degli utenti invitati da un utente e in seguito registrati
    // I: id Utente
    // O: false oppure le email degli utenti registrati
    function getPuntiCouponHistory($idReferente){

        try{
            $conn = connectToDB();

            preventSQLInjection($conn, $idReferente);

            $sql = "SELECT USER.email, USER.dataCompletamentoRegistrazione FROM USER WHERE USER.idReferral='".$idReferente."' AND USER.tipoUtente='UTENTE'";
            $result = $conn->query($sql);

            if ($result->num_rows < 1) {
                return false;
            } else {
                while($row = $result->fetch_assoc()) {
                    return $row;
                }
            }
        } finally {
            $conn->close();
        }
    }

    // estrae dati dall'utente
    // I: chiave registrazione
    // O: false oppure un oggetto contenente i dati dell'utente
    function retrieveDataReferral($chiaveRegistrazione){

        try{
            $conn = connectToDB();

            preventSQLInjection($conn, $chiaveRegistrazione);

            $sql = "SELECT USER.idUtente, USER.nome, USER.cognome, USER.idReferral FROM USER WHERE USER.chiaveRegistrazione = '".$chiaveRegistrazione."'";
            $result = $conn->query($sql);

            if ($result->num_rows < 1) {
                return false;
            } else {
                while($row = $result->fetch_assoc()) {
                    return $row;
                }
            }
        } finally {
            $conn->close();
        }

    }

?>