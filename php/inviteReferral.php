<?php

    require 'utils/commonUtils.php';
    require 'dao/dao.php';
    require 'utils/mailUtils.php';

    $nEmail=1;
    $errors=false;
    $emailInputs=array();

    //itero sulle mail inserite nel form e le metto in $emailInputs se non vi sono errori
    foreach($_POST as $key => $value) {
        if (\strpos($key, 'mailInput') !== false) {
            
            if(filter_var($value, FILTER_VALIDATE_EMAIL)){
                array_push($emailInputs, $_POST[$key]);
            } else {
                $errors=true;
                echo "<br>".'Email amico n.'.$nEmail.' non valida';                                                                            
            }

            $nEmail++;
        }
    }

    if(!$errors){
        //verifico che le mail in $emailInputs non siano presenti a DB
        foreach($emailInputs as $key => $value) {

            if(existsEmailRegistration($value)){
                $errors=true;
                echo "<br>"."L'email amico ".$value." è già presente";
            }
        }
        if(!$errors){
            $idReferral=null;
            $nomeReferral=null;
            $cognomeReferral=null;

            //estraggo dati dall'utente che poi verranno utilizzati per la mail
            $queryResult = retrieveDataReferral($_COOKIE['uuid']);

            if($queryResult==false){
                $errors=true;
                echo "ID referente non trovato";
            }else{
                foreach ($queryResult as $key => $value) {
                    if($key=="idUtente")$idReferral=$value;
                    if($key=="nome")$nomeReferral=$value;
                    if($key=="cognome")$cognomeReferral=$value;
                }
            }

            //per ogni email inserita:
            //  creo l'utenza PROSPECT-REFERRAL associata all'utente invitante e la salvo a DB
            //  invio una mail che invita a registrarsi
            foreach($emailInputs as $key => $value) {
                $couponCode = generateUIDv4(openssl_random_pseudo_bytes(16));
                $uuid = generateUIDv4(openssl_random_pseudo_bytes(16));
                $uuid = encryptData($uuid);//chiave che andrà nel link di registrazione nella mail

                $queryResult = saveUser($value, $uuid, $couponCode, "PROSPECT-REFERRAL", $idReferral);

                if($queryResult=="Success"){

                    $subject = "Sei stato invitato da ".$nomeReferral." ".$cognomeReferral;
                    $headLine = "L'utente ".$nomeReferral." ti ha invitato per offrirti: ";
                    $couponLine="Il seguente codice coupon: ".$couponCode." <br>Per ottenere sconti migliori completa la registrazione cliccando il seguente bottone!";

                    $body = getVerificationMailHTML($uuid, $subject, $headLine, $couponLine);

                    sendMail($value, $subject, $body);

                }else{
                    $errors=true;
                }
            }

            if(!$errors){
                echo 'Amici invitati, avvisali!';
            }
        }
    }
    
?>