<?php
    
    require 'utils/commonUtils.php';
    require 'dao/dao.php';
    require 'utils/mailUtils.php';

    //verifico che l'email inserita nel form sia valida
    if (array_key_exists('mailInput', $_POST) && $_POST['mailInput']!="undefined" && filter_var($_POST['mailInput'], FILTER_VALIDATE_EMAIL)){

        //controllo l'esistenza dalla mail inserita a DB
        if(existsEmailRegistration($_POST['mailInput'])){

            echo "L'email inserita è già presente";

        } else {
            //  creo l'utenza PROSPECT associata all'utente invitante e la salvo a DB
            //  invio una mail che invita a registrarsi
            $couponCode = generateUIDv4(openssl_random_pseudo_bytes(16));
            $uuid = generateUIDv4(openssl_random_pseudo_bytes(16));
            $uuid = encryptData($uuid);//chiave che andrà nel link di registrazione nella mail (criptata al fine di non renderla riconoscibile)

            $queryResult = saveUser($_POST['mailInput'], $uuid, $couponCode, "PROSPECT", "NULL");

            if($queryResult=="Success"){

                echo "Controlla la tua e-mail per verificare la registrazione!";

                $subject = "Conferma registrazione";
                $headLine = "Grazie per aver usato il nostro servizio!";
                $couponLine="Ecco il codice coupon: ".$couponCode." <br>Per ottenere sconti migliori completa la registrazione cliccando il seguente bottone!";

                $body = getVerificationMailHTML($uuid, $subject, $headLine, $couponLine);

                sendMail($_POST['mailInput'], $subject, $body);

            }else{
                echo $queryResult;
            }
            
        }

    } else {
        echo 'Email non valida';
    }

?>