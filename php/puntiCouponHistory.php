<?php

    require 'utils/commonUtils.php';
    require 'dao/dao.php';

    $id = null;
    $dataCompletamentoRegistrazioneProspect = null;

    // creo/aggiorno dei cookie che conterranno dati per lo storico dei punti guadagnati e i punti guadagnati
    $queryResult1 = retrieveRegistrationInfo($_COOKIE['uuid']);
    if($queryResult1!=false){
        foreach ($queryResult1 as $key => $value) {
            if($key=="dataCompletamentoRegistrazione") $dataCompletamentoRegistrazioneProspect=$value;
            else setcookie($key, $value, time() + (86400 * 30), "/");
        }
    }

    //estraggo l'id sulla base della chiave di registrazione
    $queryResult = retrieveDataReferral($_COOKIE['uuid']);

    if($queryResult!=false){
        foreach ($queryResult as $key => $value) {
            if($key=="idUtente")$id=$value;
        }
    }

    if($id!=null){

        //primi 100 punti guadagnati con la registrazione
        echo '('.$dataCompletamentoRegistrazioneProspect.') + 100 punti per il riscatto del coupon'."<br>";

        //estraggo le mail degli utenti e la data di registrazione, al fine di valorizzare lo storico punti guadagnati
        $queryResult2 = getPuntiCouponHistory($id);

        if($queryResult2!=false){
            echo '('.$queryResult2['dataCompletamentoRegistrazione'].') + 100 punti per la registrazione di '.$queryResult2['email']."<br>";
        }

    }else{
        echo 'Id utente non trovato';
    }
    
?>