<?php
	
	require 'utils/commonUtils.php';
	require 'dao/dao.php';
	require 'utils/securityUtils.php';

	//quando il link (dall'email) contiene la chiave di registrazione, la salvo come cookie
	if (array_key_exists('uuid', $_GET) && $_GET['uuid']!="undefined" && existsRegistrationKey($_GET['uuid'])){
		
		setcookie("uuid", $_GET['uuid'], time() + (86400 * 30), "/");

		//quando l'utente è già registrato viene reinderizzato alla pagina per invitare gli amici e visualizzare i propri punti
		//altrimenti vieni reinderizzato alla pagina di registrazione, però questa volta senza la chiave di registrazione nel link, ma bensì nel cookie
		$queryResult = retrieveRegistrationInfo($_GET['uuid']);

		if($queryResult!=false){
			header("Location: https://whappytesting.000webhostapp.com/pages/inviteReferral.html");
			exit;
		} else {
			header("Location: https://whappytesting.000webhostapp.com/pages/registration.html");
      		exit;
		}

	} else{

		// controllo l'esistenza della chiave di registrazione e riverifico che la chiave esista
		if(array_key_exists('uuid', $_COOKIE) && $_COOKIE['uuid']!="undefined" && existsRegistrationKey($_COOKIE['uuid'])){
			//controllo che i campi inseriti nel form per la registrazione siano valorizzati
			if(array_key_exists('userNome', $_POST) && $_POST['userNome']!="undefined" && $_POST['userNome']!="" && array_key_exists('userCognome', $_POST) && $_POST['userCognome']!="undefined" && $_POST['userCognome']!="" && array_key_exists('password', $_POST) && $_POST['password']!="undefined" && $_POST['password']!="" && array_key_exists('confermaPassword', $_POST) && $_POST['confermaPassword']!="undefined" && $_POST['confermaPassword']!="" ){

				if($_POST['password']==$_POST['confermaPassword']){
					
					$salt = create_salt();
    				$hashedPassword = create_hash($_POST['password'], $salt);

					//dopo aver generato il salt con il quale creo l'hash dalla password, salvo tutto a DB come UTENTE
					if(saveCompletedRegistration($_POST['userNome'], $_POST['userCognome'], $hashedPassword, $salt, $_COOKIE['uuid'])){

						echo "Registrazione avvenuta con successo!";

						$idReferente = null;

						//mi recupero l'id del referente e incremento i suoi punti
						$queryResult = retrieveDataReferral($_COOKIE['uuid']);

						if($queryResult!=false){
							foreach ($queryResult as $key => $value) {
								if($key=="idReferral")$idReferente=$value;
							}

							$puntiCouponDaAggiungere = 100;

							$queryResult1 = addPuntiCoupon($idReferente, $puntiCouponDaAggiungere);

							if($queryResult1!="Success"){
								echo $queryResult1;
							}

						}

					} else{
						echo "Utente già registrato ";
					}
				} else{
					echo "Le password non corrispondono";
				}
			} else{
				echo "Campi vuoti.";
			}
		}else{
			echo "Link di registrazione scaduto o inesistente.";
		}
	}

?>