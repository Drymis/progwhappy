<?php
    
    //genero un uuid partendo da un dato random ricevuto in input
    function generateUIDv4($data){ 
        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    //cripta dei dati senza salvare come essi vengano criptati
    function encryptData($dataToEncrypt){//$decryptedData = openssl_decrypt($encryptedData, $cypherMethod, $key, $options=0, $iv);

        $cypherMethod = 'AES-256-CBC';
        $key = random_bytes(32);
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cypherMethod));

        $encryptedData = openssl_encrypt($dataToEncrypt, $cypherMethod, $key, $options=0, $iv);

        return $encryptedData;
	}

    //adatta il link di conferma registrazione
    function encodeURIComponent($str) {
        $revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
        return strtr(rawurlencode($str), $revert);
	}
    
    //rimuove caratteri bom invisibili
	function remove_utf8_bom($text){
		$bom = pack('H*','EFBBBF');
		$text = preg_replace("/^$bom/", '', $text);
		return $text;
	}
	
?>