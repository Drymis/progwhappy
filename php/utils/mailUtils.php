<?php
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\SMTP;
	require 'PHPMailer/src/Exception.php';
	require 'PHPMailer/src/PHPMailer.php';
	require 'PHPMailer/src/SMTP.php';
	
	function sendMail($receiver, $subject, $body){

		//if implementing library with apache composer
		//require '../../vendor/autoload.php';
		
		$mail = new PHPMailer(); // create a new object
		/*$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);*/

		$mail->isSMTP(); // enable SMTP
		//$mail->Host = 'localhost';
		$mail->SMTPDebug = 0; // debugging: 0 = nothing, 1 = errors and messages, 2 = messages only
		$mail->SMTPAuth = true; // authentication enabled
		$mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
		$mail->Host = "smtp.gmail.com";
		$mail->Port = 587; // ssl 465/tls 587
		$mail->isHTML(true);
		$mail->Username = "whappymailtest@gmail.com";
		$mail->Password = "12345Lol";
		$mail->setFrom('whappymailtest@gmail.com', 'Whappy');
		$mail->addAddress($receiver);
		$mail->Subject = $subject;
		$mail->Body = $body;
		
		
		
		if(!$mail->Send()) {
			echo "Status: \nMailer Error: " . $mail->ErrorInfo;
		} 
		//else {
		//	echo "Status: \nMessage has been sent";
		//}
	}

	//crea l'email a partire da un template, adattandolo in base ai parametri ricevuti
	function getVerificationMailHTML($uuid, $subject, $headLine, $couponLine){

        $url = "https://whappytesting.000webhostapp.com/php/registration.php?uuid=".encodeURIComponent($uuid);
        
        $htmlTemplate = file_get_contents("../pages/emailTemplate.html");

		$htmlTemplate = str_replace ( "{{subject}}", $subject, $htmlTemplate);
		$htmlTemplate = str_replace ( "{{headLine}}", $headLine, $htmlTemplate);
        $htmlTemplate = str_replace ( "{{url}}", $url, $htmlTemplate);
        $htmlTemplate = str_replace ( "{{couponLine}}", $couponLine, $htmlTemplate);

        return $htmlTemplate;
        
    }

?>